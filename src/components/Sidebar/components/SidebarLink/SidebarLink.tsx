import React from 'react';
import clsx from 'clsx';
import { Link, useLocation } from 'react-router-dom';
import { MenuItem, Typography } from '@material-ui/core';
import { Props } from 'components/Sidebar/components/SidebarLink/types';
import { useStyles } from 'components/Sidebar/useStyles';

const SidebarLink: React.FC<Props> = (
  {
    to,
    icon,
    text
  }) => {
  const location = useLocation();
  const classes = useStyles();

  return <MenuItem
    component={Link}
    to={to}
    className={clsx(location.pathname === to && classes.activeLink, classes.link)}
  >
    {icon}
    <Typography variant='h4'>
      {text}
    </Typography>
  </MenuItem>;
};

export default SidebarLink;
