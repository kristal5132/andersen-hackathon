import { User } from 'types/user';

export const getFullName = (user: User) => {
  return user.firstName + ' ' + user.lastName
}
