import { TASK_STATUSES } from 'config/constants';

export const getStatusLabel = (status: TASK_STATUSES) => {
  switch (status) {
    case TASK_STATUSES.CLOSED:
      return 'Closed'
    case TASK_STATUSES.IN_PROCESS:
      return 'In Process'
    case TASK_STATUSES.OPEN:
      return 'Open'
  }
}
