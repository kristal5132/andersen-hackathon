import React from 'react';
import MyTasksTable from './MyTasksTable';
import PageLayout from 'components/PageLayout';

const MyTasks: React.FC = () => {
  return <PageLayout title="My tasks list" subtitle="Click on row for open more information">
    <MyTasksTable />
  </PageLayout>;
};

export default MyTasks;
