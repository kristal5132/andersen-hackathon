import { makeStyles } from '@material-ui/core';
import { BORDER_COLOR } from 'config/theme/palette';

export const useStyles = makeStyles((theme) => ({
  userInfoWrapper: {
    padding: theme.spacing(4, 2, 2),
    borderBottom: `1px solid ${BORDER_COLOR}`
  },
  icon: {
    marginRight: theme.spacing(3),
    color: '#757575'
  },
  avatar: {
    width: 56,
    height: 56
  },
  mainWrapper: {
    borderRight: `1px solid ${BORDER_COLOR}`,
    height: '100%'
  },
  linksWrapper: {
    padding: theme.spacing(2, 1),
    borderBottom: `1px solid ${BORDER_COLOR}`
  },
  link: {
    borderRadius: 4,
    padding: 10,
    marginBottom: theme.spacing(1)
  },
  activeLink: {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.primary.main,
    '& svg': {
      color: theme.palette.primary.main
    },
    '&:hover': {
      backgroundColor: theme.palette.primary.light,
    }
  }
}))
