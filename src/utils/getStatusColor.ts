import { TASK_STATUSES } from 'config/constants';

export const getStatusColor = (status: TASK_STATUSES) => {
  switch (status) {
    case TASK_STATUSES.CLOSED:
      return '#FB2424'
    case TASK_STATUSES.IN_PROCESS:
      return '#E4E82C'
    case TASK_STATUSES.OPEN:
      return '#1FEA64'
  }
}
