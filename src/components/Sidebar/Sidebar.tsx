import React from 'react';
import { Avatar, Box, Typography } from '@material-ui/core';
import AssignmentIcon from '@material-ui/icons/Assignment';
import PostAddIcon from '@material-ui/icons/PostAdd';
import ChatIcon from '@material-ui/icons/Chat';
import userAvatar from 'assets/images/avatar.png';
import { useAuthContext } from 'contexts/authContext';
import { ROUTES } from 'config/routes';
import { useStyles } from './useStyles';
import SidebarLink from 'components/Sidebar/components/SidebarLink';
import { refactorRoleToLabel } from 'utils/refactorRoleToLabel';

const Sidebar: React.FC = () => {
  const classes = useStyles();
  const { user } = useAuthContext();

  return (
    <Box className={classes.mainWrapper} component="aside">
      <Box className={classes.userInfoWrapper}>
        <Box display="flex" alignItems="center">
          <Avatar src={userAvatar} className={classes.avatar} />
          <Box ml={2}>
            <Typography variant="h3">
              {refactorRoleToLabel(user.role)}
            </Typography>
            <Typography color="secondary" variant="body2">
              {user.email}
            </Typography>
          </Box>

        </Box>
        <Box mt={2}>
          <Typography variant='h2'>
            {`${user.firstName} ${user.lastName}`}
          </Typography>
        </Box>
        <Box mt={1} display="flex">
          <Box mr={1}>
            <Typography color="primary">
              Balance:
            </Typography>
          </Box>
          <Typography color="primary">
            {`$${user.balance.toFixed(2)}`}
          </Typography>
        </Box>
      </Box>
      <Box className={classes.linksWrapper}>
        <SidebarLink
          to={ROUTES.ALL_TASKS}
          icon={<ChatIcon className={classes.icon} />}
          text='All Tasks'
        />
        <SidebarLink
          to={ROUTES.MY_TASKS}
          icon={<AssignmentIcon className={classes.icon} />}
          text='My Tasks'
        />
        <SidebarLink
          to={ROUTES.CREATE_TASK}
          icon={<PostAddIcon className={classes.icon} />}
          text='Create Task'
        />
      </Box>
      {/*<Box>
        <Box padding={2}>
          <Typography color='secondary'>
            Settings
          </Typography>
        </Box>
        <Box paddingX={1}>
          <SidebarLink
            to={ROUTES.SETTINGS}
            icon={<SettingsIcon className={classes.icon} />}
            text='Edit account'
          />
        </Box>
      </Box>*/}
    </Box>
  );
};

export default Sidebar;
