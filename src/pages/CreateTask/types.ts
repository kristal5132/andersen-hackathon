export type CreateTaskForm = {
  title: string;
  technology: string;
  price: string | number;
  deadline: string;
  shortDescription: string;
  description: string;
}
