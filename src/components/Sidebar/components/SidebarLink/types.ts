import React from 'react';

export type Props = {
  to: string;
  icon: React.ReactNode;
  text: string;
}
