import React from 'react';
import { Box, Grid } from '@material-ui/core';
import Sidebar from '../Sidebar';
import { useCommonStyles } from 'config/theme/commonStyles';

const Layout: React.FC = ({ children }) => {
  const commonClasses = useCommonStyles();

  return (
    <Grid container className={commonClasses.fullScreen}>
      <Grid item md={2}>
        <Sidebar />
      </Grid>
      <Grid item md={10}>
        <Box padding={4}>
          {children}
        </Box>
      </Grid>
    </Grid>
  );
};

export default Layout;
