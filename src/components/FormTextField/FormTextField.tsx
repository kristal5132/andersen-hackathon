import * as React from 'react';
import {
  FieldValues,
  useController,
  UseControllerProps
} from 'react-hook-form';
import { TextField, TextFieldProps } from '@material-ui/core';

const FormTextField = <T extends FieldValues>(
  {
    formProps,
    muiProps
  }: { formProps: UseControllerProps<T>, muiProps: TextFieldProps }) => {
  const { field } = useController(formProps);

  return (
    <div>
      <TextField {...field} {...muiProps} />
    </div>
  );
};

export default FormTextField;
