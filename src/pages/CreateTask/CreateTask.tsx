import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Box, Button, Grid, Snackbar } from '@material-ui/core';
import LabelledFormTextField from 'pages/CreateTask/LabelledFormTextField';
import PageLayout from 'components/PageLayout';
import { CreateTaskForm } from './types';
import { yupResolver } from '@hookform/resolvers/yup'
import { validationSchema } from './validationSchema';
import { Alert } from '@material-ui/lab';
import { defaultValues } from 'pages/CreateTask/utils';

const CreateTask: React.FC = () => {
  const [isOpenSnackbar, setIsOpenSnackbar] = useState(false);
  const form = useForm<CreateTaskForm>({
    defaultValues,
    resolver: yupResolver(validationSchema)
  });

  const handleChangeSnackbarState = () => {
    setIsOpenSnackbar((prevState) => !prevState)
  }

  const onSubmit = (data: CreateTaskForm) => {
    console.log(data);
    handleChangeSnackbarState()
    form.reset({...defaultValues })
  }

  return <PageLayout title='Create task'>
    <Box mt={4}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <Grid container spacing={4}>
          <Grid item xs={6}>
            <Box mb={1}>
              <LabelledFormTextField
                form={form}
                label='Type title of your task'
                textFieldLabel='Title'
                name='title'
              />
            </Box>
            <Box mb={1}>
              <LabelledFormTextField
                form={form}
                label='Type technology that used in your task'
                textFieldLabel='Technology'
                name='technology'
              />
            </Box>

            <Box mb={1}>
              <LabelledFormTextField
                form={form}
                label='Type price of task'
                textFieldLabel='Price'
                name='price'
                type="number"
              />
            </Box>

            <Box mb={1}>
              <LabelledFormTextField
                form={form}
                label='Type deadline of your task'
                textFieldLabel='Deadline'
                name='deadline'
              />
            </Box>

          </Grid>
          <Grid item xs={6}>
            <Box mb={1}>
              <LabelledFormTextField
                form={form}
                label='Type short description about your task'
                textFieldLabel='Short description'
                name='shortDescription'
                multiline
                rows={3}
              />
            </Box>
            <Box mb={1}>
              <LabelledFormTextField
                form={form}
                label='Type full description about your task'
                textFieldLabel='Description'
                name='description'
                multiline
                rows={6}
              />
            </Box>
          </Grid>
        </Grid>

        <Box display="flex" justifyContent="flex-end">
          <Button
            variant='contained'
            color='primary'
            type="submit"
          >
            Save
          </Button>
        </Box>
      </form>
      <Snackbar open={isOpenSnackbar} autoHideDuration={3000} onClose={handleChangeSnackbarState}>
        <Alert severity="success">
          Successfully created task!
        </Alert>
      </Snackbar>
    </Box>
  </PageLayout>;
};

export default CreateTask;
