export type Offer = {
  executorName: string;
  executorEmail: string;
  shortDescription: string;
  estimate: string;
  coverLetter: string;
  id: number;
  taskId: number;
}
