import { RouteProps } from 'react-router';
import AllTasks from 'pages/AllTasks';
import MyTasks from 'pages/MyTasks';
import Settings from 'pages/Settings';
import Offers from 'pages/Offers';
import Offer from 'pages/Offer/Offer';
import CreateTask from 'pages/CreateTask';

export enum ROUTES {
  ALL_TASKS = '/',
  MY_TASKS = '/my-tasks',
  SETTINGS = '/settings',
  CREATE_TASK = '/create-task',
  OFFERS = '/my-tasks/:id/offers',
  OFFER = '/my-tasks/:id/offers/:offerId'
  // LOGIN = '/login',
  // REGISTRATION = '/registration',

}

export const publicRoutes: RouteProps[] = [
  {
    component: AllTasks,
    path: ROUTES.ALL_TASKS,
    exact: true
  },
  {
    component: MyTasks,
    path: ROUTES.MY_TASKS,
    exact: true
  },
  {
    component: Settings,
    path: ROUTES.SETTINGS,
    exact: true
  },
  {
    component: Offers,
    path: ROUTES.OFFERS,
    exact: true
  },
  {
    component: Offer,
    path: ROUTES.OFFER
  },
  {
    component: CreateTask,
    path: ROUTES.CREATE_TASK,
    exact: true
  }
];
