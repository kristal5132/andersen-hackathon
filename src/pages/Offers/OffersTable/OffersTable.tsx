import React from 'react';
import {
  Avatar, Box, Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from '@material-ui/core';
import { offersMock } from 'mocks/offersMock';
import { useCommonStyles } from 'config/theme/commonStyles';
import userAvatar from 'assets/images/avatar.png';
import { useStyles } from './useStyles';
import { useHistory, useParams } from 'react-router';
import { ROUTES } from 'config/routes';
import clsx from 'clsx';

const OffersTable: React.FC = () => {
  const classes = useStyles();
  const history = useHistory();
  const { id: taskId } = useParams<{ id: string }>();
  const commonClasses = useCommonStyles();

  const handleOpenOffer = (id: number) => () => {
    history.push(`${ROUTES.MY_TASKS}/${taskId}/offers/${id}`);
  };

  return <TableContainer className={commonClasses.tableContainer}>
    <Table aria-label='collapsible table' className={commonClasses.table}>
      <TableHead>
        <TableRow>
          <TableCell className={commonClasses.tableHeadCell}>
            <Typography>Executor</Typography>
          </TableCell>
          <TableCell className={commonClasses.tableHeadCell}>
            <Typography>Short description</Typography>
          </TableCell>
          <TableCell className={commonClasses.tableHeadCell}>
            <Typography>Estimate</Typography>
          </TableCell>
          <TableCell className={clsx(commonClasses.tableHeadCell, commonClasses.minContentWidth)} />
        </TableRow>
      </TableHead>
      <TableBody>
        {offersMock.map((row) => (
          <TableRow key={row.id}>
            <TableCell>
              <Box display='flex' alignItems='center'>
                <Avatar src={userAvatar} className={classes.avatar} />
                <Box ml={1}>
                  <Typography variant='body2' color='textPrimary'>
                    {row.executorName}
                  </Typography>
                  <Typography color='secondary' variant='caption'>
                    {row.executorEmail}
                  </Typography>
                </Box>
              </Box>
            </TableCell>
            <TableCell>{row.shortDescription}</TableCell>
            <TableCell>
              {row.estimate}
            </TableCell>
            <TableCell>
              <Button
                variant='contained'
                className={classes.moreButton}
                onClick={handleOpenOffer(row.id)}
              >
                More
              </Button>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  </TableContainer>;
};

export default OffersTable;
