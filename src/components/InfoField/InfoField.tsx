import React from 'react';
import { Box, Link, Typography } from '@material-ui/core';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import { Props } from 'components/InfoField/types';
import { useStyles } from 'components/InfoField/useStyles';

const InfoField: React.FC<Props> = ({ title, subtitle, files }) => {
  const classes = useStyles();

  return (
    <>
      <Typography variant='h3' gutterBottom>
        {title}
      </Typography>
      <Typography variant="body2" color="textPrimary">
        {subtitle}
      </Typography>

      {files && files.map((file, index) => (
        <Box key={index} display="flex" alignItems="center">
          <Box mr={1}><AttachFileIcon color="primary" /></Box>
          <Link
            href='#'
            className={classes.link}
          >
            {file}
          </Link>
        </Box>
      ))}
    </>
  );
};

export default InfoField;
