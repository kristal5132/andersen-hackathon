import React from 'react';
import clsx from 'clsx';
import { Box, Collapse, Grid, TableCell, TableRow } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { useStyles } from './useStyles';
import { Props } from './types';
import InfoField from 'components/InfoField';

const CollapsibleTableRow: React.FC<Props> = ({ row, index }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();

  const handleOpenDetails = () => {
    setOpen(prevState => !prevState);
  };

  return (
    <>
      <TableRow
        className={clsx(classes.root, !open && classes.openBorder, !!(index % 2) && classes.secondaryBackground)}
        onClick={handleOpenDetails}
      >
        <TableCell>
          {row.customerName}
        </TableCell>
        <TableCell>{row.technology}</TableCell>
        <TableCell>{row.shortDescription}</TableCell>
        <TableCell>{`$${row.price.toFixed(2)}`}</TableCell>
        <TableCell className={classes.arrowCell}>
          {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell className={classes.collapsedCell} colSpan={5}>
          <Collapse in={open}>
            <Box padding={2}>
              <Grid container spacing={5}>
                <Grid item xs={6}>
                  <Box mb={3}>
                    <InfoField title="Title:" subtitle={row.title} />
                  </Box>

                  <Box>
                    <InfoField title="Description:" subtitle={row.description} />
                  </Box>
                </Grid>

                <Grid item xs={6}>
                  {row.deadline && <Box mb={3}>
                    <InfoField title="Deadline:" subtitle={row.deadline} />
                  </Box>}
                  <Box>
                    <InfoField title="Attachments:" files={['file 1', 'file 2', 'file 3']} />
                  </Box>
                </Grid>
              </Grid>

            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};

export default CollapsibleTableRow;
