import React, { useState } from 'react';
import { useHistory, useParams } from 'react-router';
import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogContent,
  Grid,
  Typography
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import PageLayout from 'components/PageLayout';
import InfoField from 'components/InfoField';
import { offersMock } from 'mocks/offersMock';
import { ROUTES } from 'config/routes';
import { useCommonStyles } from 'config/theme/commonStyles';
import { useStyles } from './useStyles';
import { hashMock } from 'mocks/hashMock';
import { useAuthContext } from 'contexts/authContext';
import { tasksMock } from 'mocks/tasksMock';

const Offer: React.FC = () => {
  const classes = useStyles();
  const commonClasses = useCommonStyles();
  const { changeUserBalance } = useAuthContext();
  const [isOpenApproveDialog, setIsOpenApproveDialog] = useState(false);
  const [isApprovedOffer, setIsApprovedOffer] = useState(false);
  const [isLoadingSmartContract, setIsLoadingSmartContract] = useState(false);
  const { offerId, id: taskId } = useParams<{ offerId: string, id: string }>();
  const history = useHistory();

  const task = tasksMock.find((task) => task.id === +taskId);
  const offer = offersMock.find((offer) => offer.id === +offerId);

  if (!offerId || !offer) {
    history.push(ROUTES.MY_TASKS);
  }

  const handleApproveOffer = () => {
    setIsApprovedOffer(true);
    setIsLoadingSmartContract(true);

    setTimeout(() => {
      setIsLoadingSmartContract(false);
      if (task?.price) {
        changeUserBalance(task.price)
      }
    }, 4000);
  };

  const handleGoBack = () => {
    history.push(`${ROUTES.MY_TASKS}/${taskId}/offers`);
  };

  const handleOpenApproveDialog = () => {
    setIsOpenApproveDialog(true);
  };

  const handleCloseApproveDialog = () => {
    setIsOpenApproveDialog(false);
  };

  const handleDeclineOffer = () => {
    handleGoBack();
  };

  return offer ? (<PageLayout title={'Offer #' + offerId}>
    <Box mt={4}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <InfoField title='Executor:' subtitle={offer!.executorName} />
        </Grid>
        <Grid item xs={12}>
          <InfoField title='Cover letter:' subtitle={offer.coverLetter} />
        </Grid>
        <Grid item xs={12}>
          <InfoField title='Estimate:' subtitle={offer.estimate} />
        </Grid>
        <Grid item xs={12}>
          <Box mt={1}>
            <Grid container justifyContent='space-between'>
              <Grid item>
                <Grid container spacing={2}>
                  <Grid item>
                    <Button
                      disabled={isApprovedOffer}
                      variant='contained'
                      color='primary'
                      onClick={handleOpenApproveDialog}
                    >
                      Approve
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button
                      disabled={isApprovedOffer}
                      variant='contained'
                      className={commonClasses.deleteButton}
                      onClick={handleDeclineOffer}
                    >
                      Decline
                    </Button>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item>
                <Button
                  variant='contained'
                  color='primary'
                  onClick={handleGoBack}
                >
                  Go Back
                </Button>
              </Grid>
            </Grid>

          </Box>
        </Grid>
      </Grid>

      <Dialog open={isOpenApproveDialog} maxWidth='md' fullWidth>
        <DialogContent className={classes.dialogContent}>
          {isApprovedOffer && !isLoadingSmartContract &&
          <Box display='flex' justifyContent='flex-end'>
            <CloseIcon
              onClick={handleCloseApproveDialog}
              className={classes.closeIcon}
            />
          </Box>}
          {!isApprovedOffer ? <> <Box display='flex' justifyContent='flex-end'>
            <CloseIcon
              onClick={handleCloseApproveDialog}
              className={classes.closeIcon}
            />
          </Box>
            <Box mb={3} width='100%'>
              <Typography variant='h2' align='center'>
                Are you sure to approve this offer ?
              </Typography>
            </Box>

            <Box mb={6} width='100%'>
              <Typography variant='h3' align='center' color="secondary">
                Offer will be processed with smart contract. The service price
                will be charged and frozen from your balance until the task is
                completed and approved. In case of cancellation of the task, the
                funds will be returned to your balance.
              </Typography>
            </Box>

            <Box>
              <Grid container spacing={4} justifyContent='center'>
                <Grid item>
                  <Button
                    variant='contained'
                    color='primary'
                    disabled={isApprovedOffer}
                    onClick={handleApproveOffer}
                  >
                    Yes
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    variant='contained'
                    className={commonClasses.deleteButton}
                    disabled={isApprovedOffer}
                    onClick={handleCloseApproveDialog}
                  >
                    No
                  </Button>
                </Grid>
              </Grid>
            </Box>
          </> : isLoadingSmartContract ? <Box pb={2}>
            <Grid container direction='column' spacing={4} alignItems='center'>
              <Grid item>
                <Typography variant='h2'>
                  Smart Contract processing...
                </Typography>
              </Grid>
              <Grid item>
                <CircularProgress />
              </Grid>
            </Grid>
          </Box> : <Box width="100%" pb={2}>
            <Box width="100%" mb={2}>
              <Typography variant="h2" align="center">
                Contract successfully created
              </Typography>
            </Box>
            <Box>
              <Typography variant="h3" gutterBottom align="center">
                your hash to check the contract:
              </Typography>
              <Typography color="primary" align="center">
                {hashMock}
              </Typography>
            </Box>
          </Box>}
        </DialogContent>
      </Dialog>
    </Box>
  </PageLayout>) : null;
};

export default Offer;
