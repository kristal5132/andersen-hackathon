import createPalette, { PaletteOptions } from '@material-ui/core/styles/createPalette';

export const BORDER_COLOR = '#E5E5E5'

const paletteOptions: PaletteOptions = {
  primary: {
    light: '#E3F2FD',
    main: '#2196F3',
    dark: '#2E69D0'
  },
  secondary: {
    main: '#9e9e9e',
    light: '#f5f5f5',
    dark: '#494949'
  },

  error: {
    main: '#FB2424',
    light: '#FF6C6C',
    dark: '#CD1E1E'
  },
  success: {
    main: '#4CAF50',
    light: '#9CCC65'
  },
  warning: {
    main: '#FFA726',
    light: '#FCF3D7'
  },
  text: {
    primary: '#494949'
  }
};

const palette = createPalette(paletteOptions);

export default palette;
