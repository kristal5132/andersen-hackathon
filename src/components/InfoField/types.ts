export type Props = {
  title: string;
  subtitle?: string;
  files?: string[];
}
