export const defaultValues = {
  title: '',
  deadline: '',
  price: '',
  description: '',
  shortDescription: '',
  technology: ''
}
