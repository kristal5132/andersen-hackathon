import React from 'react';
import AllTasksTable from 'pages/AllTasks/AllTasksTable';
import PageLayout from 'components/PageLayout';

const AllTasks: React.FC = () => {
  return <PageLayout title="My tasks list" subtitle="Click on row for open more information">
    <AllTasksTable />
  </PageLayout>
};

export default AllTasks;
