import React from 'react';
import {
  Box,
  Button,
  Collapse,
  Grid,
  TableCell,
  TableRow
} from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { useStyles } from './useStyles';
import { Props } from './types';
import clsx from 'clsx';
import InfoField from 'components/InfoField';
import { getStatusLabel } from 'utils/getStatusLabel';
import { getStatusColor } from 'utils/getStatusColor';
import { TASK_STATUSES } from 'config/constants';
import { useCommonStyles } from 'config/theme/commonStyles';
import { history } from 'config/history';
import { ROUTES } from 'config/routes';

const CollapsibleTableRow: React.FC<Props> = ({ row, index }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles({ color: getStatusColor(row.status) });
  const commonClasses = useCommonStyles();

  const handleOpenDetails = () => {
    setOpen(prevState => !prevState);
  };

  const handleOpenOffers = () => {
    history.push(`${ROUTES.MY_TASKS}/${row.id}/offers`)
  }

  return (
    <>
      <TableRow
        className={clsx(classes.root, !open && classes.openBorder, !!(index % 2) && classes.secondaryBackground)}
        onClick={handleOpenDetails}
      >
        <TableCell>
          {row.title}
        </TableCell>
        <TableCell>{row.technology}</TableCell>
        <TableCell>{row.shortDescription}</TableCell>
        <TableCell>{`$${row.price.toFixed(2)}`}</TableCell>
        <TableCell><span className={classes.circle}/>{getStatusLabel(row.status)}</TableCell>
        <TableCell className={classes.arrowCell}>
          {open ?
            <KeyboardArrowUpIcon className={classes.arrow} /> :
            <KeyboardArrowDownIcon className={classes.arrow} />
          }
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell className={classes.collapsedCell} colSpan={6}>
          <Collapse in={open}>
            <Box padding={2}>
              <Grid container spacing={5}>
                <Grid item xs={6}>
                  <Box mb={3}>
                    <InfoField title="Title:" subtitle={row.title} />
                  </Box>
                  <Box>
                    <InfoField title="Description:" subtitle={row.description} />
                  </Box>
                </Grid>

                <Grid item xs={6}>
                  {row.deadline && <Box mb={3}>
                    <InfoField title="Deadline:" subtitle={row.deadline} />
                  </Box>}
                  <Box>
                    <InfoField title="Attachments:" files={['file 1', 'file 2', 'file 3']} />
                  </Box>
                </Grid>
              </Grid>
            </Box>

            {row.status === TASK_STATUSES.OPEN && <Box padding="0 16px 32px">
              <Grid container justifyContent="space-between">
                <Grid item>
                  <Grid container spacing={2}>
                    <Grid item>
                      <Button disabled variant="contained" color="primary">
                        Edit
                      </Button>
                    </Grid>
                    <Grid item>
                      <Button disabled variant="contained" className={commonClasses.deleteButton}>
                        Delete
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Button variant="contained" color="primary" onClick={handleOpenOffers}>
                    Offers
                  </Button>
                </Grid>
              </Grid>
            </Box>}
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};

export default CollapsibleTableRow;
