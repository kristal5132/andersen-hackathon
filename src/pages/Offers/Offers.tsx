import React from 'react';
import { useHistory, useParams } from 'react-router';
import { ROUTES } from 'config/routes';
import { processedTasks } from 'mocks/tasksMock';
import PageLayout from 'components/PageLayout';
import { Box, Button, Typography } from '@material-ui/core';
import OffersTable from 'pages/Offers/OffersTable';

const Offers: React.FC = () => {
  const { id: taskId } = useParams<{ id: string }>();
  const history = useHistory();
  const task = processedTasks.find((task) => task.id === +taskId);

  if (!taskId || !task) {
    history.push(ROUTES.MY_TASKS)
  }

  const handleGoBack = () => {
    history.push(ROUTES.MY_TASKS)
  }

  return <PageLayout title={task!.title}>
    <Box mt={2}>
      <Typography variant="h3">
        Offers
      </Typography>
      <Box mt={2}>
        <OffersTable />
      </Box>
      <Box mt={2} display="flex" justifyContent="flex-end">
        <Button variant="contained" color="primary" onClick={handleGoBack}>
          Go Back
        </Button>
      </Box>
    </Box>
  </PageLayout>
};

export default Offers;
