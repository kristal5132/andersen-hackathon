import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  closeIcon: {
    cursor: 'pointer'
  },
  dialogContent: {
    padding: theme.spacing(2)
  }
}))
