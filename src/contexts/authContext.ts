import { createContext, useContext } from 'react';
import { userMock } from 'mocks/userMock';
import { User } from 'types/user';
import { ROLES } from 'config/constants';

export type AuthContext = {
  user: User;
  changeUserRole: (role: ROLES) => void;
  changeUserBalance: (amount: number) => void;
}

const defaultAuthContext: AuthContext = {
  user: userMock,
  changeUserRole: (role: ROLES) => null,
  changeUserBalance: (amount: number) => null
}

const authContext = createContext(defaultAuthContext);

export const useAuthContext = () => useContext(authContext);
export const AuthProvider = authContext.Provider;

