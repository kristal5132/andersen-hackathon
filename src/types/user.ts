import { ROLES } from 'config/constants';

export type User = {
  firstName: string;
  lastName: string;
  email: string;
  role: ROLES
  balance: number;
}
