import breakpoints from './breakpoints';
import palette from './palette';
import typography from './typography';
import { createTheme } from '@material-ui/core';

const theme = createTheme({
  direction: 'ltr',
  palette,
  typography,
  breakpoints,
  overrides: {
    MuiButton: {
      root: {
        padding: '10px 16px'
      }
    }
  }
});

export default theme;
