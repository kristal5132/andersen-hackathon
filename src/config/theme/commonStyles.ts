import { makeStyles } from '@material-ui/core';

export const useCommonStyles = makeStyles((theme) => ({
  fullScreen: {
    height: '100vh'
  },
  deleteButton: {
    backgroundColor: theme.palette.error.main,
    '&:hover': {
      backgroundColor: theme.palette.error.dark
    },
    color: theme.palette.common.white
  },
  tableHeadCell: {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.primary.main,
  },
  tableContainer: {
    borderRadius: 4,
    border: `1px solid rgba(255, 255, 255, 0,75)`
  },
  table: {
    borderCollapse: 'initial'
  },
  minContentWidth: {
    width: 0
  }
}))
