import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from '@material-ui/core';
import { tasksMock } from 'mocks/tasksMock';
import CollapsibleTableRow from 'pages/AllTasks/CollapsibleTableRow';
import { useCommonStyles } from 'config/theme/commonStyles';

const AllTasksTable: React.FC = () => {
  const classes = useCommonStyles();

  return <TableContainer className={classes.tableContainer}>
    <Table aria-label='collapsible table' className={classes.table}>
      <TableHead>
        <TableRow>
          <TableCell className={classes.tableHeadCell}>
            <Typography>Customer name</Typography>
          </TableCell>
          <TableCell className={classes.tableHeadCell}>
            <Typography>Technology</Typography>
          </TableCell>
          <TableCell className={classes.tableHeadCell}>
            <Typography>Short description</Typography>
          </TableCell>
          <TableCell className={classes.tableHeadCell}>
            <Typography>Price</Typography>
          </TableCell>
          <TableCell className={classes.tableHeadCell} />
        </TableRow>
      </TableHead>
      <TableBody>
        {tasksMock.map((row, index) => (
          <CollapsibleTableRow key={row.id} row={row} index={index} />
        ))}
      </TableBody>
    </Table>
  </TableContainer>;
};

export default AllTasksTable;
