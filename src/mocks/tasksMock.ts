import { Task } from 'types/task';
import { TASK_STATUSES } from 'config/constants';

export const tasksMock: Task[] = [
  {
    id: 1,
    customerName: 'Johhny Frister',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 150.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  },
  {
    id: 2,
    customerName: 'Johhny Frister',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 150.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  },
  {
    id: 3,
    customerName: 'Johhny Frister',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 150.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  },
  {
    id: 4,
    customerName: 'Johhny Frister',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 150.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  },
  {
    id: 5,
    customerName: 'Johhny Frister',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 150.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  },
  {
    id: 6,
    customerName: 'Johhny Frister',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 150.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  },
  {
    id: 7,
    customerName: 'Johhny Frister',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 150.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  },
  {
    id: 8,
    customerName: 'Johhny Frister',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 150.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  },
  {
    id: 9,
    customerName: 'Johhny Frister',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 150.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  },
  {
    id: 10,
    customerName: 'Johhny Frister',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 150.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  },
  {
    id: 11,
    customerName: 'Johhny Frister',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 150.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  },
  {
    id: 12,
    customerName: 'Alex Merse',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 150.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  }
];

export const processedTasks: Task[] = [
  {
    id: 12,
    customerName: 'Alex Merse',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects 1',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 111.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.OPEN
  },
  {
    id: 13,
    customerName: 'Alex Merse',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects 2',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 22.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.IN_PROCESS
  },
  {
    id: 14,
    customerName: 'Alex Merse',
    customerEmail: 'alex@company.com',
    title: 'Unusual projects 3',
    attachments: ['file1', 'file2'],
    technology: 'React',
    deadline: '2 weeks',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    price: 55.00,
    shortDescription: 'Make some magic',
    status: TASK_STATUSES.CLOSED
  }
]
