import { Path, UseFormReturn } from 'react-hook-form';

export type Props<T> = {
  label: string;
  form:  UseFormReturn<T>;
  name: Path<T>;
  textFieldLabel: string;
}
