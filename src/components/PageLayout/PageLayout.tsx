import React from 'react';
import { Box, Paper, Typography } from '@material-ui/core';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import { useStyles } from './useStyles';
import { Props } from './types';

const PageLayout: React.FC<Props> = ({ children, subtitle, title }) => {
  const classes = useStyles();

  return (
    <Box height='100%'>
      <Paper elevation={1}>
        <Box padding={2}>
          <Typography variant="h3">
            {title}
          </Typography>
          {subtitle && <Box display="flex" alignItems="center" mt={1} mb={2}>
            <InfoOutlinedIcon className={classes.infoIcon} color="secondary" />
            <Typography color="secondary">
              {subtitle}
            </Typography>
          </Box>}

          <Box mb={3}>
            {children}
          </Box>
        </Box>
      </Paper>
    </Box>
  )
}

export default PageLayout;
