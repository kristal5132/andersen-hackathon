import { makeStyles } from '@material-ui/core';
import theme from 'config/theme';

export const useStyles = makeStyles({
  root: {
    cursor: 'pointer'
  },
  secondaryBackground: {
    backgroundColor: '#F9F9FA'
  },
  openBorder: {
    '& > *': {
      borderBottom: 'unset',
    }
  },
  collapsedCell: {
    padding: 0,
    overflow: 'hidden'
  },
  arrowCell: {
    width: 0
  },
  circle: {
    display: 'inline-block',
    marginRight: 4,
    marginBottom: 1,
    width: 8,
    height: 8,
    borderRadius: '50%',
    backgroundColor: (props: { color: string }) => props.color
  },
  arrow: {
    borderRadius: '50%',
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.primary.main
  }
});
