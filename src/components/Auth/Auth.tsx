import React, { useState } from 'react';
import { AuthContext, AuthProvider } from 'contexts/authContext';
import { userMock } from 'mocks/userMock';
import { ROLES } from 'config/constants';

const Auth: React.FC = ({ children }) => {
  const [authState, setAuthState] = useState({
    user: userMock
  });

  const changeUserRole = (role: ROLES) => {
    setAuthState((prevState) => ({
      ...prevState,
      user: {
        ...prevState.user,
        role
      }
    }))
  }

  const changeUserBalance = (amount: number) => {
    if (authState.user.balance > amount) {
      setAuthState((prevState) => ({
        ...prevState,
        user: {
          ...prevState.user,
          balance: prevState.user.balance - amount
        }
      }))
    }
  }

  const authProviderValue: AuthContext = {
    ...authState,
    changeUserRole,
    changeUserBalance
  };

  return <AuthProvider value={authProviderValue}>{children}</AuthProvider>
};

export default Auth;
