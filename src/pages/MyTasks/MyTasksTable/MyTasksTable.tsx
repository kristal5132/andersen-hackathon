import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from '@material-ui/core';
import { processedTasks } from 'mocks/tasksMock';
import CollapsibleTableRow from '../CollapsibleTableRow';
import { useCommonStyles } from 'config/theme/commonStyles';

const MyTasksTable: React.FC = () => {
  const classes = useCommonStyles();

  return <TableContainer className={classes.tableContainer}>
    <Table aria-label='collapsible table' className={classes.table}>
      <TableHead>
        <TableRow>
          <TableCell className={classes.tableHeadCell}>
            <Typography>Title</Typography>
          </TableCell>
          <TableCell className={classes.tableHeadCell}>
            <Typography>Technology</Typography>
          </TableCell>
          <TableCell className={classes.tableHeadCell}>
            <Typography>Short description</Typography>
          </TableCell>
          <TableCell className={classes.tableHeadCell}>
            <Typography>Price</Typography>
          </TableCell>
          <TableCell className={classes.tableHeadCell}>
            <Typography>Status</Typography>
          </TableCell>
          <TableCell className={classes.tableHeadCell} />
        </TableRow>
      </TableHead>
      <TableBody>
        {processedTasks.map((row, index) => (
          <CollapsibleTableRow key={row.id} row={row} index={index} />
        ))}
      </TableBody>
    </Table>
  </TableContainer>;
};

export default MyTasksTable;
