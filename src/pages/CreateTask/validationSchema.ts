import * as Yup from 'yup';

const requiredString = Yup.string().required('Field is required');

export const validationSchema = Yup.object().shape({
  deadline: requiredString,
  description: requiredString,
  price: Yup.number().required('Field is required').typeError('Invalid type'),
  shortDescription: requiredString,
  technology: requiredString,
  title: requiredString
})
