import { User } from 'types/user';
import { ROLES } from 'config/constants';

export const userMock: User = {
  firstName: 'Dimitry',
  lastName: 'Kirdyaskin',
  email: 'dimitrolio@gmail.com',
  role: ROLES.CUSTOMER,
  balance: 999
}
