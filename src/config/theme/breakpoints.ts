import createBreakpoints, { BreakpointsOptions } from '@material-ui/core/styles/createBreakpoints';

const breakpointsOptions: BreakpointsOptions = {
  keys: ['xs', 'sm', 'md', 'lg', 'xl'],
  values: { xs: 0, sm: 425, md: 768, lg: 1200, xl: 1920 }
};

const breakpoints = createBreakpoints(breakpointsOptions);

export default breakpoints;
