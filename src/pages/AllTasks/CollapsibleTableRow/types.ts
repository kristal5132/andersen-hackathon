import { Task } from 'types/task';

export type Props = {
  row: Task;
  index: number;
}
