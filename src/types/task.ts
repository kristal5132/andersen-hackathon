import { TASK_STATUSES } from 'config/constants';

export type Task = {
  customerName: string;
  customerEmail: string;
  technology: string;
  shortDescription: string;
  price: number;
  title: string;
  description: string;
  deadline?: string;
  attachments?: string[]
  status: TASK_STATUSES;
  id: number;
}
