import { Offer } from 'types/offers';

export const offersMock: Offer[] = [
  {
    executorName: 'Christopher Nolan',
    executorEmail: 'chris@nolan.com',
    shortDescription: "I'll do this task faster than anyone",
    coverLetter: 'This cover letter is in regard to my interest in applying for this task. With my skill-set and competencies, I am more than able to develop all your requirements in time with high quality.',
    id: 1,
    estimate: '1w',
    taskId: 12
  },
  {
    executorName: 'Christopher Nolan',
    executorEmail: 'chris@nolan.com',
    shortDescription: "I'll do this task faster than anyone",
    coverLetter: 'This cover letter is in regard to my interest in applying for this task. With my skill-set and competencies, I am more than able to develop all your requirements in time with high quality.',
    id: 2,
    estimate: '1w',
    taskId: 12
  },
  {
    executorName: 'Christopher Nolan',
    executorEmail: 'chris@nolan.com',
    shortDescription: "I'll do this task faster than anyone",
    coverLetter: 'This cover letter is in regard to my interest in applying for this task. With my skill-set and competencies, I am more than able to develop all your requirements in time with high quality.',
    id: 3,
    estimate: '1w',
    taskId: 12
  },
  {
    executorName: 'Christopher Nolan',
    executorEmail: 'chris@nolan.com',
    shortDescription: "I'll do this task faster than anyone",
    coverLetter: 'This cover letter is in regard to my interest in applying for this task. With my skill-set and competencies, I am more than able to develop all your requirements in time with high quality.',
    id: 4,
    estimate: '1w',
    taskId: 12
  }
]
