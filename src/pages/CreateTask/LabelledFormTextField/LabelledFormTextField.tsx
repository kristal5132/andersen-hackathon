import React from 'react';
import {
  Box,
  FormHelperText,
  FormLabel,
  TextFieldProps,
  Typography
} from '@material-ui/core';
import FormTextField from 'components/FormTextField/FormTextField';
import { Props } from './types';
import { FieldError, FieldValues } from 'react-hook-form';

const LabelledFormTextField = <T extends FieldValues>(
  {
    form,
    label,
    name,
    textFieldLabel,
    ...rest
  }: Props<T> & TextFieldProps) => {
  const hasError = !!form.formState.errors[name];

  return (
    <Box mb={1}>
      <FormLabel>
        <Typography color='textPrimary'>
          {label}
        </Typography>
      </FormLabel>
      <FormTextField
        formProps={{ ...form, name }} muiProps={{
        error: hasError,
        variant: 'outlined',
        fullWidth: true,
        margin: 'normal',
        label: textFieldLabel,
        ...rest,
        helperText: hasError && <FormHelperText component="span" error={hasError}>
          {(form.formState.errors?.[name] as FieldError).message}
        </FormHelperText>
      }}
      />
    </Box>
  );
};

export default LabelledFormTextField;
