import { makeStyles } from '@material-ui/core';


export const useStyles = makeStyles({
  root: {
    cursor: 'pointer'
  },
  secondaryBackground: {
    backgroundColor: '#F9F9FA'
  },
  openBorder: {
    '& > *': {
      borderBottom: 'unset',
    }
  },
  collapsedCell: {
    padding: 0
  },
  arrowCell: {
    width: 0
  }
});
