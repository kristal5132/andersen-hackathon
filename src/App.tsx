import { MuiThemeProvider } from '@material-ui/core';
import theme from 'config/theme';
import Layout from 'components/Layout';
import Auth from 'components/Auth';
import { publicRoutes } from 'config/routes';
import { Route, Router, Switch } from 'react-router';
import { history } from 'config/history';

const App = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <Router history={history}>
        <Auth>
          <Layout>
            <Switch>
              {publicRoutes.map((route, index) => (
                <Route key={index} {...route} />
              ))}
            </Switch>
          </Layout>
        </Auth>
      </Router>
    </MuiThemeProvider>
  );
};

export default App;
