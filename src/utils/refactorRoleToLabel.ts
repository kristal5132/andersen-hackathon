import { ROLES } from 'config/constants';

export const refactorRoleToLabel = (role: ROLES) => {
  switch (role) {
    case ROLES.CUSTOMER:
      return 'Customer'
    case ROLES.EXECUTOR:
      return 'Executor'
  }
}
